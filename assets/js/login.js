$(function () {
    // 点击去注册跳转到注册页面
    $('#login').on('click', () => { //()=>{}箭头函数等价于 function(){}
        $('#login_box').hide().siblings('#reg_box').show()

    })
    // 点击去登陆跳转到登陆页面
    $('#reg').on('click', () => { //()=>{}箭头函数等价于 function(){}
        $('#reg_box').hide().siblings('#login_box').show()
    })


    // 表单验证规则
    // 从layui中获取form对象
    var form = layui.form
    // 通过 form.verify规则自定义属性
    form.verify({
        username: function (value, item) { //value：表单的值、item：表单的DOM对象
                if (!new RegExp("^[a-zA-Z0-9_\u4e00-\u9fa5\\s·]+$").test(value)) {
                    return '用户名不能有特殊字符';
                }
                if (/(^\_)|(\__)|(\_+$)/.test(value)) {
                    return '用户名首尾不能出现下划线\'_\'';
                }
                if (/^\d+\d+\d$/.test(value)) {
                    return '用户名不能全为数字';
                }
            }
            //我们既支持上述函数式的方式，也支持下述数组的形式
            //数组的两个值分别代表：[正则匹配、匹配不符时的提示文字]
            ,
        pass: [
            /^[\S]{6,12}$/, '密码必须6到12位，且不能出现空格'
        ],
        //确认密码是否一致
        repwd(value) {
            if (value != $('#reg_box [name="password"]').val()) {
                return '两次密码必须要一致！'
            }
        }
    });


    // 通过ajax监听事件进行注册
    form.on('submit(reg)', function (data) {
        $.ajax({
            type: "POST",
            url: "/api/reguser",
            data: data.field,
            success: function (res) {
                if (res.status !== 0) {
                    return layer.msg(res.message, {
                        icon: 5
                    });
                }
                layer.msg(res.message, {
                    icon: 6
                });
                // 注册成功之后返回登录页
                $('#reg_box a').click();
                // 注册之后数据自动传到登陆框输入框
                $('#login_box [name = "username"]').val($('#reg_box [name = "username"]').val())
                $('#login_box [name = "password"]').val($('#reg_box [name = "password"]').val())
            }
        });
        return false; //阻止表单跳转。如果需要表单跳转，去掉这段即可。
    });


    // 监控登陆页面
    form.on('submit(login)', function (data) {
        $.ajax({
            type: "POST",
            url: "/api/login",
            data: data.field,
            success: function (res) {
                if (res.status !== 0) {
                    return layer.msg(res.message, {
                        icon: 5
                    });
                }
                layer.msg(res.message, {
                    icon: 6
                });
                // 登陆成功之后跳转至首页
                location.href = "/index.html";
                // 登陆成功，就把token返回值储存到localstroage内
                localStorage.setItem('token', res.token);
            }
        });
        return false; //阻止表单跳转。如果需要表单跳转，去掉这段即可。
    });












})