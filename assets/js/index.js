$(function () {
    //调用 getUserInfo 获取用户信息
    getUserInfo()
    //点击退出按钮跳转login

    $('#loginOut').on('click', function () {
        layer.confirm('确定退出登录?', {
            icon: 3,
            title: '提示'
        }, function (index) {
            //do something
            // 1.清除本地token令牌数据
            localStorage.removeItem('token');
            // 2.跳转到login页面
            location.href = '/login.html'
            layer.close(index)
        });
    })

})

// 获取用户信息
function getUserInfo() {
    $.ajax({
        type: "GET",
        url: "/my/userinfo",
        // 请求头统一放置在baseAPI.js中
        // headers: {
        //     Authorization: localStorage.getItem('token') || ''
        // },
        success: function (res) {
            // console.log(res);
            if (res.status !== 0) {
                return layer.msg(res.message, {
                    icon: 5
                });
            }
            // layer.msg('登陆成功', {
            //     icon: 6
            // });
            // 调用渲染用户头像
            renderAvatar(res.data);
        }
        // complete: function (res) {
        //     console.log(res);
        //     if (res.responseJSON.status == 1 && res.responseJSON.message === '身份认证失败！') {
        //       // 1.清除本地token令牌数据
        //     localStorage.removeItem('token');
        //     // 2.跳转到login页面
        //     location.href = '/login.html'  
        //     }
        // }
    });

}

// 渲染用户头像
function renderAvatar(data) {
    let name = data.nickname || data.username;
    $('#welcome').html(`欢迎　${name}`);

    // 渲染头像
    // 有值则走if
    if (data.user_pic) {
        // 有设置头像
        $(".layui-nav-img").attr('src', data.user_pic).show();
        $(".text-avatar").hide()
    } else {
        // 没有设置头像,则显示文字图像，内容是文字的第一个字
        $(".text-avatar").html(name[0].toUpperCase()).show();
        $(".layui-nav-img").hide()
    }
}