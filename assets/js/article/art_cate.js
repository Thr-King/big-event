$(function () {
    var layer = layui.layer;
    var form = layui.form

    getInfo();

    // 获取文章分类的列表
    function getInfo() {
        $.ajax({
            type: "GET",
            url: "/my/article/cates",
            dataType: "JSON",
            success: function (res) {
                var htmlStr = template('tpl-table', res)
                $('tbody').html(htmlStr)
            }
        });
    }
    //为添加类别按钮绑定点击事件
    var indexAdd = null;
    $('#btnAssCate').on('click', function () {
        indexAdd = layer.open({
            type: 1,
            area: ['500px', '250px'],
            title: '添加文章分类',
            content: $('#dialog-add').html()

        })
    })
    //通过代理的形式，为 form-add 表单绑定 submit 事件
    $('body').on('submit', '#form-add', function (e) {
        e.preventDefault()
        $.ajax({
            type: "POST",
            url: "/my/article/addcates",
            data: $(this).serialize(), //快速获取form表单中的所有数据，使用前提：form表单并且按钮是submit，input中的要有name值，button不支持使用
            dataType: "JSON",
            success: function (response) {
                if (response.status !== 0) {
                    layer.msg('新增分类失败！')
                }
                getInfo();
                layer.msg('新增分类成功！')
                // 根据索引，关闭对应的弹出层
                layer.close(indexAdd)
            }
        });

    })


    //通过代理的形式，为 btn-edit 表单绑定事件
    $('tbody').on('click', '.btn-edit', function () {
        //弹出一个修改文章分类信息的层
        indexAdd = layer.open({
            type: 1,
            area: ['500px', '250px'],
            title: '修改文章分类',
            content: $('#dialog-edit').html()
        })
        var id = $(this).attr('data-id')
        $.ajax({
            type: "GET",
            url: '/my/article/cates/' + id,
            data: "data",
            dataType: "JSON",
            success: function (response) {
                form.val('form-edit', response.data)
            }
        });

    })

    // 通过代理的形式，为 btn-edit 绑定submit 事件

    $('body').on('submit', '#form-edit', function (e) {
        e.preventDefault()
        $.ajax({
            type: "POST",
            url: "/my/article/updatecate",
            data: $(this).serialize(),
            dataType: "JSON",
            success: function (res) {
                if (res.status !== 0) {
                    return layer.msg('更新分类数据失败！')
                }
                layer.msg('更新分类数据成功！')
                layer.close(indexAdd) //关闭弹出层
                getInfo();
            }
        });
    })

    //         form.on('submit(edit)', function (data) {
    //         $.ajax({
    //             type: "POST",
    //             url: "/my/article/updatecate",
    //             data: data.field,
    //             dataType: "JSON",
    //             success: function (res) {
    //                 if (res.status !== 0) {
    //                    return layer.msg('更新分类数据失败！')
    //                 }
    //                 layer.msg('更新分类数据成功！')
    //                 layer.close(indexAdd)//关闭弹出层
    //                 getInfo();
    //             }
    //         });
    //             return false;
    //       })

    // 通过代理的形式，为删除按钮绑定点击事件
    $('body').on('click', '.btn-delete', function () {
        var id = $(this).attr('data-id')
        layer.confirm('删除此类目?', {
            icon: 3,
            title: '提示'
        }, function (index) {
            $.ajax({
                type: "GET",
                url: '/my/article/deletecate/' + id,
                success: function (res) {
                    if (res.status !== 0) {
                        return layer.msg('删除分类失败！')
                    }
                    layer.msg('删除分类成功！')
                    layer.close(index)
                    getInfo();

                }
            });
            layer.close(index);
        });
    })

})