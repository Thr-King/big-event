$(function () {
            // 定义一个查询的参数对象，将来请求数据的时候，
            // 需要将请求参数对象提交到服务器

            var layer = layui.layer;
            var form = layui.form;
            var laypage = layui.laypage


            //定义美化时间的过滤器利用template模板做过滤器
            template.defaults.imports.date = function (date) {
                const dt = new Date(date)

                var y = dt.getFullYear();
                var m = dt.getMonth() + 1;
                var m = m < 10 ? '0' + m : m;
                var d = dt.getDate();
                var d = d < 10 ? '0' + d : d;

                var hh = dt.getHours();
                var hh = hh < 10 ? '0' + hh : hh;
                var mm = dt.getMinutes();
                var mm = mm < 10 ? '0' + mm : mm;
                var ss = dt.getSeconds(); //如果用函数进行调用时，则应该这样写 var ss = padZero(dt.getSeconds());
                var ss = ss < 10 ? '0' + ss : ss;

                return y + '-' + m + '-' + d + '　' + hh + ':' + mm + ':' + ss;

                //    或者可以定义一个补零的函数进行调用
                // function padZero(n) {
                //     return n < 10 ? '0' + n : n;
                // }

            }

            // 定义获取的data值
            var q = {
                pagenum: 1, //页码值，默认请求第一页的数据
                pagesize: 2, //每页显示几条数据，默认每页显示2条
                cate_id: '', //文章分类的id
                state: '', //文章的发布状态
            }
            //调用函数
            getlist();
            getcate();

            // 封装获取列表数据的函数
            function getlist() {
                $.ajax({
                    type: "GET",
                    url: "/my/article/list",
                    data: q,
                    dataType: "JSON",
                    success: function (res) {
                        console.log(res);
                        if (res.status !== 0) {
                            layer.msg('获取列表失败!')
                        }
                        //使用模版引擎渲染页面的数据
                        var htmlStr = template('tpl-table', res)
                        $('tbody').html(htmlStr);
                        //调用渲染分页方法
                        renderPage(res.total)
                    }
                });
            }

            // 封装获取分类及状态筛选数据
            function getcate() {
                $.ajax({
                    type: "GET",
                    url: "/my/article/cates",
                    success: function (res) {
                        if (res.status !== 0) {
                            return layer.msg('获取类别数据失败！')
                        }

                        var htmlStr = template('tpl-cate', res)
                        $('[name=cate_id]').html(htmlStr)
                        form.render();
                    }
                });

            }

            // 筛选功能
            $('#form_search').on('submit', function (e) {
                e.preventDefault()
                //获取表单中选中的值
                var cate_id = $('[name=cate_id]').val();
                var state = $('[name=state]').val();
                // 查询到的数据，赋值到 q 中对应的属性值

                q.cate_id = cate_id;
                q.state = state;

                //根据最新的筛选属性值，重新渲染页面

                getcate();

            })
            //定义渲染分页的方法
            // 定义渲染分页的方法
            function renderPage(total) {
                // 调用 laypage.render() 方法来渲染分页的结构
                laypage.render({
                    elem: 'pageBox', // 分页容器的 Id
                    count: total, // 总数据条数
                    limit: q.pagesize, // 每页显示几条数据
                    curr: q.pagenum, // 设置默认被选中的分页
                    layout: ['count', 'limit', 'prev', 'page', 'next', 'skip'],
                    limits: [2, 3, 5, 10],
                    // 分页发生切换的时候，触发 jump 回调
                    // 触发 jump 回调的方式有两种：
                    // 1. 点击页码的时候，会触发 jump 回调
                    // 2. 只要调用了 laypage.render() 方法，就会触发 jump 回调
                    jump: function (obj, first) {
                        // 可以通过 first 的值，来判断是通过哪种方式，触发的 jump 回调
                        // 如果 first 的值为 true，证明是方式2触发的
                        // 否则就是方式1触发的
                        // console.log(first)
                        // console.log(obj.curr)
                        // 把最新的页码值，赋值到 q 这个查询参数对象中
                        q.pagenum = obj.curr
                        // 把最新的条目数，赋值到 q 这个查询参数对象的 pagesize 属性中
                        q.pagesize = obj.limit
                        // 根据最新的 q 获取对应的数据列表，并渲染表格
                        // getlist();
                        if (!first) {
                            getlist();
                        }
                    }
                });

            }

            // 删除文章功能
            //通过代理的方法绑定事件
            $('tbody').on('click', '.btn-del', function () {
                var len = $('.btn-del').length
                console.log(len);
                var id = $(this).attr('data-id')
                // console.log(id);
                        layer.confirm('确认删除此文章?', {
                            icon: 3,
                            title: '提示'
                        }, function (index) {
                            $.ajax({
                                type: "GET",
                                url: "/my/article/delete/" + id,
                                data:q,
                                dataType: "JSON",
                                success: function (res) {
                                    // console.log(id);
                                    if (res.status !== 0) {
                                        return layer.msg('删除文章失败！')
                                    }
                                    layer.msg('删除文章成功！')
                                    // 当数据删除完成后，先判断这一页中是否还有剩余的数据
                                    // 如果没有数据了，则让页码-1之后再调用渲染函数
                                    if (len === 1) {
                                        // 如果len的值=1时，则删除完毕之后，页面上没有数据了
                                        q.pagenum = q.pagenum === 1 ? 1 : q.pagenum - 1
                                    }
                                    getlist();
                                }
                            });
                            layer.close(index);
                        });
                    })




            })