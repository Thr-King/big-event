$(function () {
    var form = layui.form;
    form.verify({
        pwd: [
            /^[\S]{6,12}$/, '密码必须6到12位，且不能出现空格'
        ],
        samePwd: function (value) {
            if (value == $('[name="oldPwd"]').val()) {
                return '新密码不能和原密码相同！'
            }
        },
        rePwd(value) {
            if (value != $('[name="newPwd"]').val()) {
                return '两次密码不一致！'
            }
        }
    });

    // 发送更新密码
    // 监听表单的提交行为
    form.on('submit(uppwd)', function (data) {
        $.ajax({
            type: "POST",
            url: "/my/updatepwd",
            data: data.field,
            success: function (res) {
                if (res.status !== 0) {
                    return layer.msg(res.message, {
                        icon: 5
                    });
                }
                //重新发送请求从服务器拿数据渲染
                layer.msg(res.message, {
                    icon: 6
                })
            }
        });
        return false; //阻止表单跳转。如果需要表单跳转，去掉这段即可。
    });

})