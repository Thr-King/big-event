$(function () {
    let form = layui.form;
    //表单验证
    form.verify({
        nickname: [
            /^[\S]{1,6}$/, '昵称长度必须1到6位'
        ]
    });
    // 调用
    initUserInfo();

    //   获取用户信息  
    function initUserInfo() {
        $.ajax({
            type: "GET",
            url: "/my/userinfo",
            success: function (res) {
                // console.log(res);
                if (res.status !== 0) {
                    return layer.msg(res.message, {
                        icon: 5
                    });
                }
                console.log(res);
                // 将服务器传回来的值赋值给指定输入框
                form.val('userInfo', res.data);
            }

        });

    }
    // 重置数据
    $('[type="reset"]').on('click', function (e) {
        e.preventDefault(); //阻止默认重置功能
        // 调用
        initUserInfo();
    })
})

// 监听表单的提交行为
let form = layui.form;
form.on('submit(updata)', function (data) {
    $.ajax({
        type: "POST",
        url: "/my/userinfo",
        data: data.field,
        success: function (res) {
            if (res.status !== 0) {
                return layer.msg(res.message, {
                    icon: 5
                });
            }
            //重新发送请求从服务器拿数据渲染
            window.parent.getUserInfo();
        }
    });
    console.log(data.field) //当前容器的全部表单字段，名值对形式：{name: value}
    return false; //阻止表单跳转。如果需要表单跳转，去掉这段即可。
});