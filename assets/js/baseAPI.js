// ajax请求拦截器
$.ajaxPrefilter((option) => {
    option.url = 'http://ajax.frontend.itheima.net' + option.url
    // Headers 请求头统一用配置
    if (option.url.indexOf('/my/') != -1) {
        option.headers = {
            Authorization: localStorage.getItem('token') || ''
        }
    }
    option.complete = function (res) {
        // console.log(res);
        if (res.responseJSON.status == 1 && res.responseJSON.message === '身份认证失败！') {
            // 1.清除本地token令牌数据
            localStorage.removeItem('token');
            // 2.跳转到login页面
            location.href = '/login.html'
        }
    }

})